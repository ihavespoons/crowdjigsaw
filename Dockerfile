FROM ubuntu:latest

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y tzdata
RUN ln -fs /usr/share/zoneinfo/Australia/Brisbane /etc/localtime
RUN dpkg-reconfigure --frontend noninteractive tzdata
RUN apt install git mongodb bash curl redis-server -y
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt install nodejs -y

WORKDIR /srv
COPY . CrowdJigsaw/
RUN adduser jigsaw
RUN chown -R jigsaw:jigsaw /home/jigsaw
WORKDIR /srv/CrowdJigsaw
ADD startup.sh /startup.sh
RUN mkdir /srv/CrowdJigsaw/database
RUN chmod +x /startup.sh
RUN chown -R jigsaw:jigsaw /srv/CrowdJigsaw
USER jigsaw
RUN npm install
RUN npm audit

WORKDIR /srv/CrowdJigsaw
ENV NODE_ENV=development
ENTRYPOINT /startup.sh